﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	//Used for singleton
	public static InputManager IM;

    //variables to hold the keycodes for the controller commands and can be accessed from most scripts
	public KeyCode jump {get; set;}
	public KeyCode forward {get; set;}
	public KeyCode backward {get; set;}
	public KeyCode left {get; set;}
	public KeyCode right {get; set;}



	void Awake()
	{
		//deon't destroy if only one otherwise destroy copy
		if(IM == null)
		{
			DontDestroyOnLoad(gameObject);
			IM = this;
		}	
		else if(IM != this)
		{
			Destroy(gameObject);
		}

         //assigns each variable to a keycode that is in the player prefs save location so they can retrieve it when they start a new game
         //default key assigned by hte second parameter
		jump = (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("jumpKey", "Space"));
		forward = (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("forwardKey", "W"));
		backward = (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("backwardKey", "S"));
		left = (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("leftKey", "A"));
		right = (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("rightKey", "D"));

	}

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}
}
