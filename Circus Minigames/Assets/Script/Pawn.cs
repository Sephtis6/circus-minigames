﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{

    [HideInInspector] public HumanController CC;
    [HideInInspector] public Transform tf;

    //integers and floats named
    [HideInInspector] public Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;

    public void Start()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        CC.pawn = GetComponent<Pawn>();
        tf = GetComponent<Transform>();

        // Get my components
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    public void Update()
    {

    }

    public void Jump(float jumpForce)
    {
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
    }
}