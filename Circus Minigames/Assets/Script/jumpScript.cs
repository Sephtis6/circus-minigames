﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpScript : MonoBehaviour
{
    public HumanController CC;
    [HideInInspector] public Rigidbody2D rb;
    public float jumpForce;
    public Transform tf;

    // Use this for initialization
    void Start ()
    {
        CC = GameObject.Find("CharaacterController").GetComponent<HumanController>();
        tf = GetComponent<Transform>();
        CC.SetScore();
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
	}

    public void Jump()
    {
        tf.position += tf.up * jumpForce;
    }
}
