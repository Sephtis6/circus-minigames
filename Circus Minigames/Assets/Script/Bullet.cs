﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public HumanController CC;
    public Transform tf;
    public float speed;

    // Use this for initialization
    void Start ()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        tf = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Move();
    }

    //destroys the bullet and the object the bullet hits
    public void OnTriggerEnter2D(Collider2D otherCollider)
    {
        AddScore(otherCollider.gameObject.GetComponent<hitScript>().scoreToAdd);
        Destroy(this.gameObject);
    }
    //has the bullet move in the direction it is facing when fired at a set speed.
    public void Move()
    {
        tf.position += tf.up * speed;
    }

    public void AddScore(float scoreToAdd)
    {
        CC.score = CC.score + scoreToAdd;
        CC.SetScore();
    }

    public void OnBecomeInvisible()
    {
        Destroy(gameObject);
    }

}
