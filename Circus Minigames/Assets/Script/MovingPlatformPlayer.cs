﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MovingPlatformPlayer : MonoBehaviour
{

    float dirx, movespeed = 10;
    public bool moveRight;
    public float rightEdge;
    public float leftEdge;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > rightEdge)
        {
            moveRight = false;
        }
        if (transform.position.x < leftEdge)
        {
            moveRight = true;
        }
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + movespeed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x - movespeed * Time.deltaTime, transform.position.y);
        }
    }
}
