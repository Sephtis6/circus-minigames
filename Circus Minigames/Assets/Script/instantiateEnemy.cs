﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instantiateEnemy : MonoBehaviour
{
    public int EnemySpawned;
    public float timeTillSpawn;
    public float timeUntilNextSpawn;
    public List<GameObject> objectsToSpawn;
    public GameObject spawnedObject;
    public Transform tf;

	// Use this for initialization
	void Start ()
    {
        timeUntilNextSpawn = timeTillSpawn;
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timeUntilNextSpawn -= Time.deltaTime;
        if (timeUntilNextSpawn <= 0)
        {
            Spawn();
        }
	}

    public void Spawn()
    {
        EnemySpawned = Random.Range(0, objectsToSpawn.Count);
        spawnedObject = Instantiate(objectsToSpawn[EnemySpawned], tf.position, tf.rotation);
        timeUntilNextSpawn = timeTillSpawn; 
    }
}
