﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanController : Controller {

    public int loseSceneToLoad;
    public int winningSceneToLoad;
    public float winningScore;

    public Transform tf;
    public float speed;
    public FollowPlayerCamera FPC;

    public float score;
    public Text ScoreText;


    [HideInInspector] public GameManager gm;

    public float groundDistance = 0.1f;

    public float ShotsLeft;
    public Text shotsLeftText;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        //setPawn();
        gm.volumeAudio.clip = gm.audioClips[1];
        gm.volumeAudio.loop = true;
        gm.volumeAudio.Play();
        SetShotsLeft();
        SetScore();
    }

    // Update is called once per frame
    void Update()
    {
        if (score >= winningScore)
        {
            SceneManager.LoadScene(winningSceneToLoad);
        }
        if (ShotsLeft <= 0)
        {
            SceneManager.LoadScene(loseSceneToLoad);
        }
    }

    public void SetScore()
    {
        if (score <= 0)
        {
            score = 0;
        }
        ScoreText.text = "Score: " + score.ToString();
    }

    public void SetShotsLeft()
    {
        shotsLeftText.text = " Shots Left: " + ShotsLeft.ToString();
    }

}
