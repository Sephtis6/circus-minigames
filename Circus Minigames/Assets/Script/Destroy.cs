﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Destroy : MonoBehaviour
{
    public HumanController CC;
    public float score;
    public bool pickUpAble;
    public int loseLevel;

    // Use this for initialization
    void Start()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (pickUpAble == false)
        {
            SceneManager.LoadScene(loseLevel);
        }
        if (pickUpAble == true)
        {
            AddScore();
            Destroy(gameObject);
        }
    }

    public void AddScore()
    {
        CC.score = CC.score + score;
        CC.SetScore();
    }

    public void OnBecomeInvisible()
    {
        Destroy(gameObject);
    }
}
