﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform shootPoint;
    public HumanController CC;


    // Use this for initialization
    void Start()
    {
        CC = GameObject.Find("CharaacterController").GetComponent<HumanController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //spawns a bullet if space is pressed
        if (Input.GetKeyDown(KeyCode.Space) & CC.ShotsLeft > 0)
        {
            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
            CC.ShotsLeft -= 1;
            CC.SetShotsLeft();
        }

    }
}
